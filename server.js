var connect = require('connect');
var serveStatic = require('serve-static');

connect().use(
	serveStatic("./")
).listen(process.env.PORT);